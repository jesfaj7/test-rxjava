package com.neuvoo.rxjava;

import com.neuvoo.bean.ExecuteCommand;
import com.neuvoo.pojo.ParamsPojo;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RxAppNeuvoo {
	public static void main(String args[]) {
	    int nJson = 43;
	    int salary = 85000;
        ParamsPojo paramsPojo1 = new ParamsPojo(nJson, salary, 1);
        ParamsPojo paramsPojo2 = new ParamsPojo(nJson, salary, 2);
        ParamsPojo paramsPojo3 = new ParamsPojo(nJson, salary, 3);
        ParamsPojo paramsPojo4 = new ParamsPojo(nJson, salary, 4);
        ParamsPojo paramsPojo5 = new ParamsPojo(nJson, salary, 5);
        ParamsPojo paramsPojo6 = new ParamsPojo(nJson, salary, 6);

        Observer<ParamsPojo> observer = new Observer<ParamsPojo>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(ParamsPojo paramsPojo) {
                System.out.println(paramsPojo.toString());
                String command = "casperjs src/com/neuvoo/resources/tax_se.js " +
                        paramsPojo.getNJson() + " " +
                        paramsPojo.getSalary() + " " +
                        paramsPojo.getNLote();
                ExecuteCommand.callCommand(command);
            }

            @Override
            public void onError(Throwable e) {
                System.out.println("onError: " + e.getStackTrace());
            }

            @Override
            public void onComplete() {
                System.out.println("onComplete: All Done!");
            }
        };

        Observable.just(paramsPojo1,
                        paramsPojo2,
                        paramsPojo3,
                        paramsPojo4,
                        paramsPojo5,
                        paramsPojo6)
                   .subscribe(observer);
    }
}
