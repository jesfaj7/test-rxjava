/**
 * @file Extract taxes from 'https://statsskuld.se/en-sv/jobs/berakna-nettolon'.
 * @author Farid Ayaach
 * @edited by Jesus Fajardo
 * @version 0.3
 */

/*************************************************************************************/
/* INITIALIZATION ********************************************************************/
/*************************************************************************************/

// environment
var fs     = require('fs');
var utils  = require('utils');
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,     // The WebPage instance used by Casper will
        loadPlugins: false,     // use these settings
        userAgent:   'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
    },
    viewportSize: {
        width:       1020,
        height:      600
    },
    logLevel:        "debug",   // Only "error" level messages will be logged
    verbose:         false,     // log messages will be printed out to the console
    retryTimeout:    50,
    waitTimeout:     100,
});

var number = casper.cli.get(0);
var param_salary = casper.cli.get(1);
var lote = casper.cli.get(2);

// general
var mainurl          = 'https://statsskuld.se/en-sv/jobs/berakna-nettolon';
var filename         = 'src/com/neuvoo/resources/json/tax_se' + "_" + number + "_" + lote +  ".json";
var pattern_comma    = /(\,)/g;//Regex for character comma
var pattern_dot      = /(\.)/g;//Regex for character dot
var pattern_currency = /\skr/;//Regex for currency character or word

// salaries
var salaries         = [param_salary

    // "1",      "20",     "50",     "70",     "100",    "200",    "400",    "600",    "800",    "900",
    // "1000",   "2000",   "3000",   "4000",   "5000",   "6000",   "7000",   "8000",   "9000",   "10000",
    // "11000",  "12000",  "13000",  "14000",  "15000",  "16000",  "17000",  "18000",  "19000",  "20000",
    // "25000",  "30000",  "35000",  "40000",  "45000",  "50000",  "55000",  "60000",  "65000",  "70000",
    // "75000",  "80000",  "85000",  "90000",  "95000",  "100000", "110000", "120000", "130000", "140000",
    // "150000", "160000", "170000", "180000", "190000", "200000", "300000", "400000", "500000", "600000",

    // "700000", "800000", "900000", "1000000"

];

var links      = [];
var arrayNames = [];

var links_aux      = [];
var arrayNames_aux = [];

var indexUrl = 0;
var region = "";

// excluded words
var wordsBlacklist = [
];

// if there are any, get values from CLI
if(casper.cli.get('filename')){
    filename = casper.cli.get('filename');
}

/*************************************************************************************/
/* PROCESSING ************************************************************************/
/*************************************************************************************/

casper.start(mainurl, function() {
    fs.write(filename, '', 'w');//Init the json file
    // get link list
    this.waitForSelector('#dataTable');

    this.then(function then_getLinks() {
        links_aux = this.getElementsAttribute('#dataTable > tbody > tr > td > a', 'href');
        arrayNames_aux = this.evaluate(getFromDOM, "#dataTable > tbody > tr > td > a");
        // for(index in links){
        //     console.log(links[index]);
        // }
        if(lote == "0") {
            links_aux.splice(26,1);
            links      = links_aux.slice(0,1);
            arrayNames = arrayNames_aux.slice(0,1);
            this.echo("Lote de links a procesar: 1");
            this.echo("Cantidad: " + links.length);
        }else if(lote == "1"){
            links_aux.splice(26,1);
            links      = links_aux.slice(0,51);
            arrayNames = arrayNames_aux.slice(0,51);
            this.echo("Lote de links a procesar: 1");
            this.echo("Cantidad: " + links.length);
        } else if(lote == "2"){
            links_aux.splice(26,1);
            links      = links_aux.slice(51,101);
            arrayNames =arrayNames_aux.slice(51,101);
            this.echo("Lote de links a procesar: 2");
            this.echo("Cantidad: " + links.length);
        } else if(lote == "3"){
            links_aux.splice(26,1);
            links      = links_aux.slice(101,151);
            arrayNames =arrayNames_aux.slice(101,151);
            this.echo("Lote de links a procesar: 3");
            this.echo("Cantidad: " + links.length);
        } else if(lote == "4"){
            links_aux.splice(26,1);
            links      = links_aux.slice(151,201);
            arrayNames =arrayNames_aux.slice(151,201);
            this.echo("Lote de links a procesar: 4");
            this.echo("Cantidad: " + links.length);
        } else if(lote == "5"){
            links_aux.splice(26,1);
            links      = links_aux.slice(201,251);
            arrayNames =arrayNames_aux.slice(201,251);
            this.echo("Lote de links a procesar: 5");
            this.echo("Cantidad: " + links.length);
        } else if(lote == "6"){
            links_aux.splice(26,1);
            links      = links_aux.slice(251);
            arrayNames =arrayNames_aux.slice(251);
            this.echo("Lote de links a procesar: 6");
            this.echo("Cantidad: " + links.length);
        }
    });

});

casper.then(function() {

    links.splice(26,1);
    for(i = 0; i< links.length; i++){
        console.log("Link: "+ (i+1) +  " > " + links[i]);
    }
    console.log("\n");

    // initialize (and truncate) the output file
    fs.write(filename, '[', 'w');

    // for every link...
    this.eachThen(links, function eachThen_links(response) {

        var salaryPos = 0;
        var link      = 'https://statsskuld.se' + response.data;
        console.log("Working with link -> " + link);

        // for each salary...
        this.eachThen(salaries, function eachThen_salaries(response) {

            var salary = response.data;
            console.log("\tWorking with salary -> " + salary);
            this.open(link.replace("med-31400-kr", "med-" + salary +  "-kr"));
            salaryPos++;

            // wait for form...
            this.waitForSelector('div.row.p-t-20',

                function then_waitForForm() {

                    this.waitForSelector('div.row.p-t-20',

                        function then_waitForResults() {
                            // get labels and values
                            labels = this.evaluate(getFromDOM, "div.col-md-9 > table > tbody > tr > td:nth-of-type(1)");
                            values = this.evaluate(getFromDOM, "div.col-md-9 > table > tbody > tr > td:nth-of-type(2)");

                            // discard table headings
                            labels.shift();
                            values.shift();

                            // clean labels and values
                            labels     = labels.map(clean);
                            values     = values.map(clean);

                            // save the data to the output file
                            region = arrayNames[indexUrl];
                            var data   = formatAsJSON(region, salary, 2018, labels, values);

                            this.reload();
                            fs.write(filename, data + ',', 'a');
                            console.log("\t\tInformation extrated. Adding to the file " + filename);

                        },

                        function onTimeout_waitForResults() {
                            // results not OK
                            this.echo('>> Skipping "' + region + '" because of results timeout...');
                            this.capture('results-timeout.png');
                            this.reload();
                        }

                    );

                },

                function onTimeout_waitForForm() {
                    // form not OK
                    this.echo('>> Skipping "' + link + '" because of form timeout...');
                    //this.capture('form-timeout.png');
                    this.reload();
                }
            );
        });
        this.echo('[< URL nro. : ' + indexUrl + ' >]');
        indexUrl++;
    });

});

/*************************************************************************************/
/* CLEANING **************************************************************************/
/*************************************************************************************/

casper.run(function() {

    fs.write(filename, ']', 'a');
    // json_data = fs.read(filename);
    // json_data = json_data.replace("}},]", "}}]");
    // fs.write(filename, json_data, 'w');
    this.echo('Done!', 'INFO');
    this.exit(0);

});

/*************************************************************************************/
/* FUNCTION DEFINITIONS **************************************************************/
/*************************************************************************************/

/**
 * Builds a minified JSON formatted string based on the parameters.
 * The return string would be like:
 * @example
 * // {
 * //     "id": "NULL",
 * //     "salary": "0",
 * //     "country": "CA",
 * //     "year": "2015",
 * //     "region": "Alberta",
 * //     "data": {
 * //                 "CPP": "0.00",
 * //                 "EI": "0.00",
 * //                 ...
 * //             }
 * // }
 *
 * Notice that is assumed that 'labels.length' === 'values.length'.
 * Empty labels[i] will be omitted.
 *
 * @param   {string}   region - The state name
 * @param   {string}   salary - The current salary
 * @param   {string[]} labels - Labels to the JSON parameters
 * @param   {string[]} values - JSON Values
 * @returns {string}            The minified formatted JSON string
 */
function formatAsJSON(region, salary, year, labels, values) {
    var output  = '\n{';
    output += '\n"frequency":"Yearly",';
    output += '\n"salary":"' + salary + '",';
    output += '\n"country":"SE",';
    output += '\n"year":"' + year + '",';
    output += '\n"region":"' + region + '",';
    output += '\n"data":{';

    //Delete the duplicates
    var array_dupl = search_duplicate(labels);
    // logToConsole("\n\n" + array_dupl + "\n\n");
    for(i = 0; i < array_dupl.length; i++){
        index_to_delete = array_dupl[i];
        labels.splice(index_to_delete, 1);
        values.splice(index_to_delete, 1);
    }

    for (var i = 0; i < labels.length; i++) {

        var label = labels[i].trim();
        var value = values[i].trim();

        if (label.length > 0) {
            output += '\n\t\t"' + label + '":"' + value + '",';
        }

    }

    // remove the last comma and close brackets
    output  = output.slice(0,-1);
    output += '}\n}';

    return output;
}

/**
 * Check if a word is in the excluded list, without using case-sensitivity.
 *
 * @param   {string}   str - Any string
 * @returns {boolean}        'str' without newlines, tabs, double-spaces, ...
 */
function clean(str) {
    return str.replace(/\r|\n|\t|\$/g, '')
        .replace(/&nbsp;/gi, '')
        .replace("<strong>", "")
        .replace("</strong>", "")
        .replace("(public service)", "")
        .replace(":", "").replace(pattern_currency,"")
        .replace(pattern_dot, "")
        .replace(pattern_comma, "")
        .replace("<strong>", "")
        .replace("</strong>", "")
        .replace("-", "");
}

/**
 * Check if a word is in the excluded list, without using case-sensitivity.
 *
 * @param   {string}   labels     - Any string array
 * @param   {string}   values     - Any string array
 * @returns {boolean}               If a 'labels[i]' matches any 'wordsBlacklist[i]', returns 'false'.
 */
function removeExcluded(labels, values) {

    var patterns = wordsBlacklist;

    for (var i = 0; i < patterns.length; i++) {

        var regex = new RegExp(patterns[i], 'i');

        for (var j = 0; j < labels.length; j++) {

            if (regex.test(labels[j]) || regex.test(values[j])) {
                labels.splice(j, 1);
                values.splice(j, 1);
            }

        }

    }

    return true;

}

/**
 * Queries DOM for a given selector and gets it's innerHTML.
 *
 * @param   {string} selector - The selector to query.
 * @returns {Array}             An array of HTML DOM elements
 */
function getFromDOM(selector) {

    var elements = document.querySelectorAll(selector);

    return Array.prototype.map.call(elements, function(e) {
        return e.innerHTML;
    });

}

/**
 * Logs a message to console
 *
 * @param {string} message - The message to log.
 */
function logToConsole(message) {
    console.log(message);
}

/**
 * Search duplicates array and return the index of everyone
 *
 * @param {Array} array_basic - Array with information
 * @returns {Array} An array of index of duplicate elements
 */
function search_duplicate(array_basic) {
    var index_array = [];
    Array.prototype.contains = function(elem) {
        for (var i in this) {
            if (this[i] == elem) return true;
        }
        return false;
    }

    for(i = 0; i < array_basic.length; i++){//Loop for get the item to search
        item_i = array_basic[i];
        for(j = i+1; j < array_basic.length; j++){//Loop for get the item to compare
            item_j = array_basic[j];
            if(item_i == item_j){
                exist = index_array.contains(j)//Validate if the index not exist in the final index array
                if(!exist){
                    index_array.push(j);
                }
            }
        }
    }

    return index_array;
}