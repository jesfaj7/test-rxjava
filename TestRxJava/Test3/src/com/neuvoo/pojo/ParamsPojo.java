package com.neuvoo.pojo;

public class ParamsPojo {
    private Integer nJson;
    private Integer salary;
    private Integer nLote;

    public ParamsPojo() {
    }

    public ParamsPojo(Integer nJson, Integer salary, Integer nLote) {
        this.nJson = nJson;
        this.salary = salary;
        this.nLote = nLote;
    }

    public Integer getNJson() {
        return nJson;
    }

    public void setNJson(Integer n_json) {
        this.nJson = n_json;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getNLote() {
        return nLote;
    }

    public void setNLote(Integer n_salary) {
        this.nLote = n_salary;
    }

    @Override
    public String toString() {
        return "ParamsPojo{" +
                "nJson=" + nJson +
                ", salary=" + salary +
                ", nLote=" + nLote +
                '}';
    }
}
