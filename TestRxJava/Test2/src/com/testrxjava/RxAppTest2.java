package com.testrxjava;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RxAppTest2 {
	public static void main(String args[]) {
        Observable<Integer> observable = Observable.create(new ObservableOnSubscribe<Integer>() {
               @Override
               public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                   //Use onNext to emit each item in the stream//
                   e.onNext(1);
                   e.onNext(2);
                   e.onNext(3);
                   e.onNext(4);
                   //Once the Observable has emitted all items in the sequence, call onComplete//
                   e.onComplete();
               }
           }
        );

        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                System.out.println("onSubscribe: ");
            }

            @Override
            public void onNext(Integer value) {
                System.out.println("onNext: " + value);
            }

            @Override
            public void onError(Throwable e) {
                System.out.println("onError: ");
            }

            @Override
            public void onComplete() {
                System.out.println("onComplete: All Done!");
            }
        };

        //Create our subscription
        observable.subscribe(observer);

        Observable.just(4, 5, 6 ).filter(x -> x % 2 == 0).subscribe(observer);
    }
}
