package com.testrxjava;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Action1;


public class RxAppTest1 {
	private List<Integer> lista1;
	private List<Integer> lista2;

	public RxAppTest1() {
		lista1 = new ArrayList<>();
		lista2 = new ArrayList<>();
		this.llenarListas();
	}
	
	void llenarListas() {
		for(int i = 0 ; i < 20; i++) {
			lista1.add(i);
			lista2.add(i);
		}
	}
	
	void buscar() {
		Observable<Integer> obs1 = Observable.from(lista1);
		Observable<Integer> obs2 = Observable.from(lista2);
		
		
		//Usando la interfaz Action1
		System.out.println("=====Usando la interfaz Action1========");
		Observable.merge(obs1, obs2).subscribe(new Action1<Integer>() {
			@Override
			public void call(Integer elm) {
				if((elm % 2) == 0) {
					System.out.println(elm);					
				}
			}			
		});
		
		//Usando expresiones lambda
		System.out.println("=====Usando expresiones lambda========");
		Observable.merge(obs1, obs2).filter(x-> x % 2 == 0).subscribe(System.out::println);

		//Usando expresiones lambda con cuerpo
		System.out.println("=====Usando expresiones lambda con cuerpo========");
		Observable.merge(obs1, obs2).filter(x-> x % 2 == 0).subscribe(x-> {
			if(x == 1) {
				System.out.println(x);									
			}
		});
}

	public static void main(String arg[]) {
		/*
		List<String> lista = new ArrayList<>();
		lista.add("Jesus");
		lista.add("Ana");
		lista.add("Alanis");
		
		Observable<String> obs = Observable.from(lista);
		obs.subscribe(new Action1<String>() {
			@Override
			public void call(String elemento) {
				System.out.println(elemento);
			}
			
		});
		*/
		RxAppTest1 rxAppTest1 = new RxAppTest1();
		rxAppTest1.buscar();
	}
}
